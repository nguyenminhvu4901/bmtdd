<?php

use App\Http\Controllers\PhoneController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('phones')->group(function () {
    Route::get('/', [PhoneController::class, 'index'])->name('phones.index');
    Route::get('/show/{id}', [PhoneController::class, 'show'])->name('phones.show');
    Route::get('/create', [PhoneController::class, 'create'])->name('phones.create')->middleware('auth');
    Route::post('/', [PhoneController::class, 'store'])->name('phones.store')->middleware('auth');
    Route::get('/edit/{id}', [PhoneController::class, 'edit'])->name('phones.edit')->middleware('auth');
    Route::put('/{id}', [PhoneController::class, 'update'])->name('phones.update')->middleware('auth');
    Route::delete('/{id}', [PhoneController::class, 'destroy'])->name('phones.destroy')->middleware('auth');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
