<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Phone;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeletePhoneTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_delete_phone_if_phone_is_exist()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->create()->toArray();
        $response = $this->delete(route('phones.destroy', ['id' => $phone['id']]));

        $response->assertRedirect('phones.index');
        $this->assertDatabaseMissing('phones', $phone);
    }

    /** @test */
    public function unauthenticate_user_can_not_delete_phone()
    {
        $phone = Phone::factory()->create()->toArray();
        $response = $this->delete(route('phones.destroy', ['id' => $phone['id']]));

        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_not_delete_phone_if_phone_is_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $phone = -1;
        $response = $this->delete(route('phones.destroy', ['id' => $phone]));

        $response->assertStatus(404);
    }
}
