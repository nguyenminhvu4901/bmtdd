<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Phone;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdatePhoneTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_update_phone_if_phone_is_valid()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->create();
        $response = $this->from(route('phones.edit', ['id' => $phone->id]))->put(route('phones.update', ['id' => $phone->id]), $phone->toArray());

        $response->assertRedirect('phones.index');
        $this->assertDatabaseHas('phones', $phone->toArray());
    }

    /** @test */
    public function authenticate_user_can_not_update_phone_if_phone_is_not_valid()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->create(['name' => '', 'category' => '']);
        $response = $this->from(route('phones.edit', ['id' => $phone->id]))->put(route('phones.update', ['id' => $phone->id]), $phone->toArray());

        $response->assertRedirect('/phones/edit/' . $phone->id);
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['name', 'category']);
    }

    /** @test */
    public function authenticate_user_can_not_update_phone_if_name_is_not_valid()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->create(['name' => '']);
        $response = $this->from(route('phones.edit', ['id' => $phone->id]))->put(route('phones.update', ['id' => $phone->id]), $phone->toArray());

        $response->assertRedirect('/phones/edit/' . $phone->id);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_phone_if_category_is_not_valid()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->create(['category' => '']);
        $response = $this->from(route('phones.edit', ['id' => $phone->id]))->put(route('phones.update', ['id' => $phone->id]), $phone->toArray());

        $response->assertRedirect('/phones/edit/' . $phone->id);
        $response->assertSessionHasErrors(['category']);
    }

    /** @test */
    public function unauthenticate_user_can_not_update_phone()
    {
        $phone = Phone::factory()->create();
        $response = $this->from(route('phones.edit', ['id' => $phone->id]))->put(route('phones.update', ['id' => $phone->id]), $phone->toArray());

        $response->assertRedirect('/login');
    }

     /** @test */
     public function unauthenticate_user_can_not_see_update_phone_view()
     {
         $phone = Phone::factory()->create();
         $response = $this->get(route('phones.edit', ['id' => $phone->id]));
 
         $response->assertRedirect('/login');
     }

     /** @test */
     public function authenticate_user_can_see_update_phone_view()
     {
        $this->actingAs(User::factory()->create());
         $phone = Phone::factory()->create();
         $response = $this->get(route('phones.edit', ['id' => $phone->id]));
 
         $response->assertViewHas('phone', $phone);
     }

     /** @test */
     public function authenticate_user_can_not_see_update_phone_view_if_phone_is_not_exist()
     {
        $this->actingAs(User::factory()->create());
         $phone = -1;
         $response = $this->get(route('phones.edit', ['id' => $phone]));
 
         $response->assertStatus(404);
     }
}
