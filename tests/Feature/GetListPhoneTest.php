<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Phone;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GetListPhoneTest extends TestCase
{
    /** @test */
    public function user_can_show_phone_if_phone_is_exist()
    {
        $phone = Phone::factory()->create();
        $response = $this->get(route('phones.index'));

        $response->assertStatus(200);
        $response->assertViewIs('phones.index');
        $response->assertSee($phone->get);
    }
}
