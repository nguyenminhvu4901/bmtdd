<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Phone;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateNewPhoneTest extends TestCase
{
    public function getCreatePhoneRoute()
    {
        return route('phones.store');
    }

    public function getCreatePhoneViewRoute()
    {
        return route('phones.create');
    }

    /** @test */
    public function authenticate_user_can_create_new_phone_if_phone_is_valid()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->make()->toArray();
        $response = $this->from($this->getCreatePhoneViewRoute())->post($this->getCreatePhoneRoute(), $phone);

        $response->assertStatus(302);
        $response->assertRedirect(route('phones.index'));
        $this->assertDatabaseHas('phones', $phone);
    }

    /** @test */
    public function unauthenticate_user_can_not_create_new_phone()
    {
        $phone = Phone::factory()->make()->toArray();
        $response = $this->from($this->getCreatePhoneViewRoute())->post($this->getCreatePhoneRoute(), $phone);

        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_not_create_new_phone_if_phone_is_not_valid()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->make(['name' => '', 'category' => ''])->toArray();
        $response = $this->from($this->getCreatePhoneViewRoute())->post($this->getCreatePhoneRoute(), $phone);

        $response->assertRedirect('phones/create');
        $response->assertSessionHasErrors(['name', 'category']);
    }

    /** @test */
    public function authenticate_user_can_not_create_new_phone_if_name_is_not_valid()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->make(['name' => ''])->toArray();
        $response = $this->from($this->getCreatePhoneViewRoute())->post($this->getCreatePhoneRoute(), $phone);

        $response->assertRedirect('phones/create');
        $response->assertSessionHasErrors(['name']);
    }


    /** @test */
    public function authenticate_user_can_not_create_new_phone_if_category_is_not_valid()
    {
        $this->actingAs(User::factory()->create());
        $phone = Phone::factory()->make(['category' => ''])->toArray();
        $response = $this->from($this->getCreatePhoneViewRoute())->post($this->getCreatePhoneRoute(), $phone);

        $response->assertRedirect('phones/create');
        $response->assertSessionHasErrors(['category']);
    }

    /** @test */
    public function unauthenticate_user_can_not_see_create_new_phone_view()
    {
        $response = $this->get($this->getCreatePhoneViewRoute());

        $response->assertRedirect('/login');
    }

     /** @test */
     public function authenticate_user_can_see_create_new_phone_view()
     {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreatePhoneViewRoute());
 
        $response->assertViewIs('phones.create');
     }
}
