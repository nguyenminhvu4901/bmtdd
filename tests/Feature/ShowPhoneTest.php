<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Phone;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowPhoneTest extends TestCase
{
    /** @test */
    public function user_can_show_phone_if_phone_is_exist()
    {
        $phone = Phone::factory()->create();
        $response = $this->get(route('phones.show', ['id' => $phone->id]));

        $response->assertStatus(200);
        $response->assertViewHas('phone', $phone);
    }

    /** @test */
    public function user_can_not_show_phone_if_phone_is_not_exist()
    {
        $phone = -1;
        $response = $this->get(route('phones.show', ['id' => $phone]));

        $response->assertStatus(404);
    }
}
