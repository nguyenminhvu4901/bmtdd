<?php

namespace Database\Factories;

use App\Models\Phone;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhoneFactory extends Factory
{
    use WithFaker;

    protected $model = Phone::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'category' => $this->faker->text,
        ];
    }
}
